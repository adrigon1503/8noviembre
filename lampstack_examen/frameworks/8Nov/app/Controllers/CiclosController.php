<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

/**
 * Description of CiclosController
 *
 * @author a073102708z
 */
class CiclosController extends BaseController{
    public function formInsertCiclo() {
        $data['title'] = 'Introducir Ciclos';
        return view('alumno/formCiclo',$data);
    }
    
    public function insertCiclo() {
        $id = $this->request->getPost('id');
        $nombre = $this->request->getPost('nombre');
        $familia = $this->request->getPost('familia');
        $grado = $this->request->getPost('grado');
        
        $grado_nuevo = [
            'id' => $id,
            'nombre' => $nombre,
            'familia' => $familia,
            'grado' => $grado,
        ];
        echo '<pre>';
        print_r($grado_nuevo);
        echo '</pre>';
    }
}
